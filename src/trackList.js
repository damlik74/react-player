import triple from './music/Twiztid-Triple_Threat.mp3';
import circles from './music/Twiztid-Circles.mp3';
import weDontDie from './music/Twiztid_ICP-We-Dont-Die.mp3';

export const trackList = [
    {
        author: "Twiztid",
        trackName: "triple-threat",
        url: triple,
    },
    {
        author: "Twiztid",
        trackName: "Circles",
        url: circles,
    },
    {
        author: "Twiztid",
        trackName: "We Don't Die",
        url: weDontDie,
    }
]
