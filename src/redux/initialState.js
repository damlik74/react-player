export const initialState = {
    currentTrackIndex: 0,
    volume: 0.5,
    tracksOrder: null,
    playState: 'pause',
    repeatState: false
};