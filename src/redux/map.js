import {bindActionCreators} from "redux";
import {changeVolume, setTracksOrder, swapTrack, setPlayState, setNextTrack, setPrevTrack, toggleRepeatState} from "./actions";

export const mapStateToProps = (component) => {
    switch(component) {
        case "Player": {
            return function (state) {
                return {
                    currentTrackIndex: state.currentTrackIndex,
                    volume: state.volume,
                    tracksOrder: state.tracksOrder,
                    playState: state.playState,
                    repeatState: state.repeatState
                };
            }
        }
        case "Playlist": {
            return function (state) {
                return {
                    currentTrackIndex: state.currentTrackIndex,
                    tracksOrder: state.tracksOrder,
                    playState: state.playState
                };
            }
        }
        case "Controls": {
            return function (state) {
                return {
                    currentTrackIndex: state.currentTrackIndex,
                    tracksOrder: state.tracksOrder,
                    playState: state.playState,
                    repeatState: state.repeatState
                };
            }
        }
        default: return undefined;
    }
}

export const mapDispatchToProps = (component) => {
    switch(component) {
        case "Player": return function(dispatch) {
            return {
                changeVolume: bindActionCreators(changeVolume, dispatch),
                swapTrack: bindActionCreators(swapTrack, dispatch),
                setTracksOrder: bindActionCreators(setTracksOrder, dispatch),
                setPlayState: bindActionCreators(setPlayState, dispatch),
                setNextTrack: bindActionCreators(setNextTrack, dispatch),
                setPrevTrack: bindActionCreators(setPrevTrack, dispatch),
                toggleRepeatState: bindActionCreators(toggleRepeatState, dispatch)
            };
        };
        case "Playlist": return function(dispatch) {
            return {
                swapTrack: bindActionCreators(swapTrack, dispatch),
            };
        };
        case "Controls": return function(dispatch) {
            return {
                swapTrack: bindActionCreators(swapTrack, dispatch),
                setTracksOrder: bindActionCreators(setTracksOrder, dispatch),
                setPlayState: bindActionCreators(setPlayState, dispatch),
                setNextTrack: bindActionCreators(setNextTrack, dispatch),
                setPrevTrack: bindActionCreators(setPrevTrack, dispatch),
                toggleRepeatState: bindActionCreators(toggleRepeatState, dispatch)
            };
        };
        default: return undefined;
    }
}