export const rootReducer = (state, action) => {
    switch (action.type) {
        case 'CHANGE_VOLUME':
            return {
                ...state,
                volume: action.payload
            }
        case 'SWAP_TRACK':
            return {
                ...state,
                currentTrackIndex: action.payload
            }
        case 'SET_TRACKS_ORDER':
            return {
                ...state,
                tracksOrder: action.payload
            }
        case 'SET_PLAY_STATE':
            return {
                ...state,
                playState: action.payload
            }
        case 'TOOGLE_REPEAT_STATE':
            return {
                ...state,
                repeatState: action.payload
            }
        default: return state
    }
};