export const changeVolume = (volume) => {
    return {
        type: 'CHANGE_VOLUME',
        payload: volume
    }
}

export const setNextTrack = ( currentTrackIndex, tracksOrder ) => {

    let currentTrackIndexInString = String( currentTrackIndex );
    let CurrentTrackIndexInOrder = tracksOrder.indexOf( currentTrackIndexInString );
    let nextIndex = CurrentTrackIndexInOrder + 1;

    if ( nextIndex >= tracksOrder.length ) {
        nextIndex = 0;
    }

    let getTracksOrderItem = tracksOrder[nextIndex];

    return swapTrack( getTracksOrderItem );

}

export const setPrevTrack = ( currentTrackIndex, tracksOrder ) => {

    let currentTrackIndexInString = String( currentTrackIndex );
    let CurrentTrackIndexInOrder = tracksOrder.indexOf( currentTrackIndexInString );
    let prevIndex = CurrentTrackIndexInOrder - 1;

    if ( prevIndex < 0 ) {
        prevIndex = tracksOrder.length - 1;
    }

    let getTracksOrderItem = tracksOrder[prevIndex];

    return swapTrack( getTracksOrderItem );

}

export const swapTrack = (trackIndex) => {
    return {
        type: 'SWAP_TRACK',
        payload: trackIndex,
    }
}

export const toggleRepeatState = (repeatState) => {
    return {
        type: 'TOOGLE_REPEAT_STATE',
        payload: repeatState
    }
}

export const setTracksOrder = (tracksOrder) => {
    return {
        type: 'SET_TRACKS_ORDER',
        payload: tracksOrder
    }
}

export const setPlayState = (playState) => {
    return {
        type: 'SET_PLAY_STATE',
        payload: playState
    }
}