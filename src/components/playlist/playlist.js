import React from "react";
import './playlist.css';
import { trackInfoContext } from "../../context";

export class Playlist extends React.Component {
    static contextType = trackInfoContext;

    swapTrack(trackIndex) {
        this.props.swapTrack(trackIndex);
    }

    renderPlaylist = () => {
        let trackList = [];
        let tracksOrder = this.getTracksOrder()

        for(let trackIndex of tracksOrder) {
            trackList.push(this.renderTrack(trackIndex));
        }

        return trackList;
    }

    renderTrack = (trackIndex) => {
        let trackItem = this.getTrackItem(trackIndex);

        return (<div key={trackIndex}>
            {trackItem.author + " - " + trackItem.trackName}
        </div>);
    }

    getTracksOrder = () => {
        return this.props.tracksOrder || Object.keys(this.context)
    }

    getTrackItem = (index) => {
        return this.context[index];
    }

    render() {
        return (
            <div className="Playlist">
                {this.renderPlaylist()}
            </div>
        );
    }
}


