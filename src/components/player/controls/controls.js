import React from "react";
import "./controls.css";
import {trackInfoContext} from "../../../context";

export class Controls extends React.Component{
    static contextType = trackInfoContext;

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="controls">
                <button
                    onClick={this.tooglePlayState.bind(this)}
                    className={this.getClassNameFromPlayState()}
                >
                    play
                </button>
                <button className="previous" onClick={this.setPrevTrack}></button>
                <button className="next" onClick={this.setNextTrack}></button>
                <button className="shuffle" onClick={this.shuffleTracksOrder}></button>
                <button className="repeat" onClick={this.toggleRepeatState.bind(this)}></button>
            </div>
        );
    }

    tooglePlayState() {
        this.props.audioRef.current.paused ? this.playAudio() : this.pauseAudio();
    }

    playAudio = () => {
        this.props.setPlayState('play');
    }

    pauseAudio = () => {
        this.props.setPlayState('pause');
    }

    getClassNameFromPlayState() {
        switch (this.props.playState) {
            case 'play' : {
                return 'pause';
            }
            case 'pause' : {
                return 'play';
            }
            default:
                return 'play';
        }
    }

    setNextTrack = () => {
        this.props.setNextTrack( this.props.currentTrackIndex, this.getTracksOrder() );
    }

    setPrevTrack = () => {
        this.props.setPrevTrack( this.props.currentTrackIndex, this.getTracksOrder() );
    }

    toggleRepeatState() {
        this.props.toggleRepeatState( !this.props.repeatState )
    }

    shuffleTracksOrder = () => {
        let currentOrder = this.getTracksOrder();
        for (let i = this.getTracksCount() - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1)); // случайный индекс от 0 до i
            [currentOrder[i], currentOrder[j]] = [currentOrder[j], currentOrder[i]];
        }
        this.setTracksOrder( currentOrder );
    }


    setTracksOrder = (tracksOrder) => {
        this.props.setTracksOrder(tracksOrder);
    }

    getTracksOrder = () => {
        let tracksOrder = this.props.tracksOrder
                            ? this.props.tracksOrder.slice()
                            : Object.keys(this.context);
        return tracksOrder;
    }

    getTracksCount = () => {
        return this.context.length;
    }

}