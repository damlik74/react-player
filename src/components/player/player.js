import React from "react";
import './player.css';
import { trackInfoContext } from "../../context";
import { Controls } from "./controls/controls";
import { connect, Provider } from 'react-redux';
import { store } from "../../redux/store";
import { mapDispatchToProps, mapStateToProps } from "../../redux/map";

const WrappedControlsComponent = connect(mapStateToProps("Controls"), mapDispatchToProps("Controls"))(Controls);

export class Player extends React.Component {
    static contextType = trackInfoContext;

    constructor(props) {
        super(props);

        this.audioRef = React.createRef();
        this.timelineRef = React.createRef();
        this.bufferRef = React.createRef();
        this.currentTimeRef = React.createRef();
        this.durationTimeRef = React.createRef();

    }

    render() {
        let currentTrack = this.getCurrentTrack();

        return (
            <div className="player">
                <audio onEnded={this.repeatOrNext.bind(this)} ref={this.audioRef} src={currentTrack.url} className="player"></audio>
                <div className="top">
                    <div className="inner">
                        <Provider store={store}>
                            <WrappedControlsComponent
                                audioRef={this.audioRef}
                            />
                        </Provider>
                        <div className="time current"><p ref={this.currentTimeRef}></p></div>
                        <div className="time duration"><p ref={this.durationTimeRef}></p></div>
                        <div className="info">
                            <p>
                                <span className="artist_name">{currentTrack.author} - </span>
                                <span className="song_name">{currentTrack.trackName}</span>
                            </p>
                        </div>
                        <div className="volume">
                            <input
                               type="range"
                               onInput={this.updateVolumeFromInput}
                               value={this.props.volume}
                               min="0"
                               max="1"
                               step="0.01"
                            />
                        </div>
                    </div>
                </div>
                <div className="bottom">
                    <div className="inner">
                        <div className="timeline">
                            <div className="buffered" ref={this.bufferRef}>
                            </div>
                            <input
                               ref={this.timelineRef}
                               type="range"
                               step="0.1"
                               min="0"
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    componentDidMount() {
        this.timelineRef.current.value = this.getCurrentAudioTime();
        this.timelineRef.current.oninput = this.updateAudioTimeFromTimeLine;

        this.setCurrentTimeFromAudio();

        this.audioRef.current.onloadedmetadata = this.setSongDurationToHTML;
        this.audioRef.current.addEventListener('timeupdate', this.updateTimeLine.bind(this));
    }

    componentDidUpdate() {
        this.tooglePlayTrack()
    }

    tooglePlayTrack = () => {
        if( this.props.playState === "play" ) {
            this.audioRef.current.play()
        } else {
            this.audioRef.current.pause()
        }
    }

    setSongDurationToHTML = () => {
        let songDuration = this.getAudioDuration();

        this.timelineRef.current.max = songDuration;
        this.durationTimeRef.current.textContent = this.convertDurationToString(songDuration);
    }

    updateTimeLine = () => {
        this.setCurrentTimeFromAudio();
        this.timelineRef.current.value = this.getCurrentAudioTime();
        this.updateBufferWidth();
    }

    updateBufferWidth = () => {
        if (this.audioRef.current.buffered.length) {
            let load_percent = (this.getCurrentAudioTime() / 2) + 1;
            this.bufferRef.current.style.width = load_percent + "%";
        }
    }

    setCurrentTimeFromAudio = () => {
        let currentSongTime = this.getCurrentAudioTime();
        this.currentTimeRef.current.textContent = this.convertDurationToString( currentSongTime );
    }

    updateAudioTimeFromTimeLine = () => {
        this.updateAudioTime( this.timelineRef.current.value )
    }

    updateVolumeFromInput = (e) => {
        let newVolume = e.target.value;
        this.updateVolume(newVolume);
    }

    updateAudioTime = (newCurrentTime) => {
        this.audioRef.current.currentTime = newCurrentTime;
    }

    updateVolume = (newVolume) => {
        this.props.changeVolume(newVolume);
        this.audioRef.current.volume = newVolume;
    }

    convertDurationToString(seconds) {
        var h = Math.floor(seconds / 3600);
        var m = Math.floor(seconds % 3600 / 60);
        var s = Math.floor(seconds % 3600 % 60);

        var hDisplay = h > 0 ? h + ":" : "";
        var mDisplay = m > 0 ? ("0" + m).slice(-2) + ":" : "00:";
        var sDisplay = s > 0 ? ("0" + s).slice(-2) : "00";

        return hDisplay + mDisplay + sDisplay;
    };

    repeatOrNext() {
        if(this.props.repeatState) {
            this.audioRef.current.currentTime = 0;
            this.audioRef.current.play()
        } else {
            this.props.setNextTrack( this.props.currentTrackIndex, this.getTracksOrder());
        }
    }

    getTracksOrder = () => {
        let tracksOrder = this.props.tracksOrder
            ? this.props.tracksOrder.slice()
            : Object.keys(this.context);
        return tracksOrder;
    }

    getCurrentTrack = () => {
        return this.context[ this.props.currentTrackIndex ];
    }

    getCurrentAudioTime = () => {
        return this.audioRef.current.currentTime.toFixed(1);
    }

    getAudioDuration = () => {
        return this.audioRef.current.duration;
    }

}